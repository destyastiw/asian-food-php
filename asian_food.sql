-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 02, 2022 at 03:07 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `asian_food`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id` int(10) UNSIGNED NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `full_name`, `username`, `password`) VALUES
(3, 'Hasya Diah', 'Hasya', '1c211e18fdaa595f4367201ce69f73c4'),
(4, 'Depyta Ilawati', 'depy', '12412c2d35f43e5cf3491f98d592d2bf'),
(5, 'Melati Puti', 'putri', '906e96bdafbefe7bfca28b471f7d9180'),
(6, 'Destyasti Widi', 'Asti', '21232f297a57a5a743894a0e4a801fc3'),
(9, 'Muhammad Abraham', 'Abraham', '6e8f3ad153f8ce760a45b007669aa1f9'),
(10, 'Cristian Daniel', 'Daniel', '61a293a4936845d31e010e4990c7b2cb');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) NOT NULL,
  `image_name` varchar(225) NOT NULL,
  `featured` varchar(10) NOT NULL,
  `active` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`id`, `title`, `image_name`, `featured`, `active`) VALUES
(2, 'Opor Ayam', 'Food_Category_698.jpg', 'No', 'Yes'),
(3, 'Kimbab Sayur', 'Asian_Food_Category_397.jpg', 'Yes', 'Yes'),
(4, 'Ayam Pandan', 'Asian_Food_Category_935.jpg', 'No', 'Yes'),
(5, 'Tinutuan', 'Food_Category_290.jpg', 'No', 'Yes'),
(6, 'Ramen Daging', 'Asian_Food_Category_11.jpg', 'Yes', 'Yes'),
(7, 'Bibimbab', 'Asian_Food_Category_137.jpg', 'Yes', 'Yes'),
(8, 'Martabak Manis', 'Asian_Food_Category_107.jpg', 'Yes', 'Yes'),
(9, 'Jajangmyeon', 'Asian_Food_Category_46.jpg', 'No', 'Yes'),
(10, 'Niku Udon', 'Asian_Food_Category_589.jpg', 'Yes', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_food`
--

CREATE TABLE `tbl_food` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(150) NOT NULL,
  `description` varchar(255) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `featured` varchar(10) NOT NULL,
  `active` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_food`
--

INSERT INTO `tbl_food` (`id`, `title`, `description`, `price`, `image_name`, `category_id`, `featured`, `active`) VALUES
(1, 'Bibimbab', 'bibimbap terdiri atas semangkuk nasi putih dengan lauk pauk di atasnya berupa sayur mayur, daging sapi, telur, dan juga pasta cabai gochujang.', '3.00', 'Food-Name-7381.jpg', 7, 'Yes', 'Yes'),
(2, 'Ramen Daging', 'makanan khas negara Jepang yang terbuat dari bahan dasar berupa mie yang berkuah. Dengan kaldu tulang dan potongan daging sapi', '2.00', 'Food-Name-5136.jpg', 6, 'Yes', 'Yes'),
(3, 'Tinutuan', 'Tinutuan merupakan campuran berbagai macam sayuran, tidak mengandung daging.', '1.00', 'Food-Name-9589.jpg', 2, 'No', 'Yes'),
(4, 'Ayam Pandan', 'Daging ayam dibungkus dengan daun pandan lalu digoreng, sehingga aromanya harum dan rasanya gurih juicy karena sari dagingnya tertahan oleh daun pandan.', '3.00', 'Food-Name-8594.jpg', 4, 'No', 'Yes'),
(5, 'Opor Ayam', 'Ayam rebus yang dimasak dengan santan yang ditambahkan dengan bermacam-macam bumbu seperti serai, kencur,dsb.', '2.00', 'Food-Name-4895.jpg', 2, 'No', 'Yes'),
(6, 'Kimbab Sayur', 'Nasi yang dibungkus dengan rumput laut. Didalamnya terdapat isian seperti sayuran dan daging.', '3.00', 'Food-Name-7799.jpg', 3, 'Yes', 'Yes'),
(10, 'Martabak Manis', 'Martabak berbentuk seperti potongan pizza. Di atasnya ditaburi ragam topping yang terdiri dari 8 rasa\r\n', '4.00', 'Food-Name-8017.jpg', 8, 'Yes', 'No'),
(11, 'Niku Udon', 'Mie tebal dari Jepang yang disajikan dalam kuah kaldu panas dan dingin.', '3.00', 'Food-Name-2593.jpg', 10, 'Yes', 'Yes'),
(12, 'Jajangmyeon', 'Jajangmyeon adalah mie dengan saus pasta kacang kedelai hitam', '3.00', 'Food-Name-7396.jpg', 9, 'Yes', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

CREATE TABLE `tbl_order` (
  `id` int(10) UNSIGNED NOT NULL,
  `food` varchar(150) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `order_date` datetime NOT NULL,
  `status` varchar(50) NOT NULL,
  `customer_name` varchar(150) NOT NULL,
  `customer_contact` varchar(20) NOT NULL,
  `customer_email` varchar(150) NOT NULL,
  `customer_address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_order`
--

INSERT INTO `tbl_order` (`id`, `food`, `price`, `qty`, `total`, `order_date`, `status`, `customer_name`, `customer_contact`, `customer_email`, `customer_address`) VALUES
(1, 'Opor Ayam', '2.00', 1, '2.00', '2022-02-01 04:07:48', 'Delivered', 'Melati Putri', '085390913454', 'put@gmail.com', 'Jalan Singgahan no 16, RT 07/RW 03.'),
(2, 'Bibimbab', '3.00', 1, '3.00', '2022-02-01 04:40:57', 'On Delivery', 'Adinda Mawar', '085390913454', 'mwr@gmail.com', 'Jl. Petemon IV No.32-A, RT 014/RW 008 (Gang Sebelah Bank BCA)');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_food`
--
ALTER TABLE `tbl_food`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_food`
--
ALTER TABLE `tbl_food`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_order`
--
ALTER TABLE `tbl_order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
