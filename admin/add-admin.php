<?php include('partials/menu.php'); ?>


<div class="main-content">
    <div class="wrapper">
        <h1>Add Admin</h1>

        <br><br>

        <?php
        if (isset($_SESSION['add'])) //Checking whether the session is Set of Not
        {
            echo $_SESSION['add']; //Display the session message if set
            unset($_SESSION['add']); //Remove session message
        }
        ?>

        <form action="" method="POST">

            <table class="tbl-30">
                <tr>
                    <td>Full Name:</td>
                    <td>
                        <input type="text" name="full_name" placeholder="Enter Your Name">
                    </td>
                </tr>

                <tr>
                    <td>Username:</td>
                    <td>
                        <input type="text" name="username" placeholder="Your Username">
                    </td>
                </tr>

                <tr>
                    <td>Password:</td>
                    <td>
                        <input type="password" name="password" placeholder="Your Password">
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <input type="submit" name="submit" value="Add Admin" class="btn-secondary">
                    </td>
                </tr>

            </table>

        </form>


    </div>
</div>

<?php include('partials/footer.php'); ?>


<?php
//process the value from and save it in Database

//Check whether the submit button is clicked or not

if (isset($_POST['submit'])) {
    // Button Clicked
    //echo "Button Clicked";

    //1.Get the data from form
    $full_name = $_POST['full_name'];
    $username = $_POST['username'];
    $password = md5($_POST['password']); //Password Encryption with MD5

    //2.SQL Query to save the data into database
    $sql = "INSERT INTO tbl_admin SET
            full_name='$full_name',
            username='$username',
            password='$password'
        ";
    $conn = mysqli_connect('localhost', 'root', '') or die(mysqli_error());
    $db_select = mysqli_select_db($conn, 'asian_food') or die(mysqli_error());

    //3. executing query and saving data into dt
    $res = mysqli_query($conn, $sql) or die(mysqli_error());

    //4. check whether the(query is executed) data is inserted or not and display appropriate message
    if ($res == TRUE) {
        //Data insert
        //echo "data inserted";
        //create a sesion variabel to display message
        $_SESSION['add'] = "<div class='success'> Admin Added Successfully. </div>";
        //Redirect Page to Manage Admin
        header("location:" . SITEURL . 'admin/manage-admin.php');
    } else {
        //failed to insert data
        //echo "failed to insert data";
        //Create a Session Variable to Display Message
        $_SESSION['add'] = "<div class='error'> Failed to Add Admin. </div>";
        //Redirect Page to Add Admin
        header("location:" . SITEURL . 'admin/add-admin.php');
    }
}

?>